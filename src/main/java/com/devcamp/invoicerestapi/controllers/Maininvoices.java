package com.devcamp.invoicerestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Maininvoices {
    @GetMapping("/invoices")
    public ArrayList<InvoiceItem> invoiceItems() {
        ArrayList<InvoiceItem> invoiceItems = new ArrayList<>();
        InvoiceItem invoiceItem1 = new InvoiceItem("abc123", "MayNuocNong", 12, 125.5);
        InvoiceItem invoiceItem2 = new InvoiceItem("abc124", "MayNuocLanh", 10, 120.5);
        InvoiceItem invoiceItem3 = new InvoiceItem("abc125", "MayLanh", 5, 185.5);
        invoiceItems.add(invoiceItem1);
        invoiceItems.add(invoiceItem2);
        invoiceItems.add(invoiceItem3);
        System.out.println(invoiceItems.toString());
        return invoiceItems;
    }

}
